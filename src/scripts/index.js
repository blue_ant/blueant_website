let $slideshow = $(".CustomSlider");

new Fitblock($slideshow.find(".Fitblock"));

let sliderMouseTracker = new CursorSmoothTracker(window);

let miniSlider;

let slideshow = new CustomSlider(
	$slideshow,
	{
		slideChangeDuration: 1.3,
		onProgressBarEnd: () => {
			miniSlider.$el.trigger("click");
		}
	},
	sliderMouseTracker
);

{
	let isTransit = false;
	miniSlider = new MiniSlider($slideshow.find(".MiniSlider"), {
		onClick: event => {
			event.preventDefault();
			if (isTransit) return false;
			isTransit = true;
			slideshow.slideNext();
			miniSlider.slideNext();
			setTimeout(() => {
				isTransit = false;
			}, 1301);
		}
	});
}

mainMenu.$el
	.on("mainmenushow", () => {
		slideshow.pause();
	})
	.on("mainmenuhide", () => {
		slideshow.play();
	});

let $moreBtn = $slideshow.find("#readMoreAboutSlideBtn");
$moreBtn.one("click", event => {
	event.preventDefault();
	mainMenu.$el.off("mainmenushow mainmenuhide");
	miniSlider.destroy();
	sliderMouseTracker.destroy();

	let projectPageUrl = slideshow.$el
		.find(".CustomSlider_slideInfo")
		.eq([slideshow.activeIndex])
		.data("project-page-url");

	slideshow.destroy();
	setTimeout(() => {
		let progressCircle = Circles.create({
			id: "circles-1",
			radius: 21,
			width: 3,
			text: null,
			colors: ["rgba(0,0,0,0)", "#0071e0"],
			duration: null,
			styleWrapper: true
		});

		let progress = {
			value: 0
		};

		TweenLite.to(progress, 2, {
			value: 100,
			// ease: Linear.easeNone,
			onUpdate: () => {
				progressCircle.update(progress.value);
			},
			onComplete: () => {
				window.location.href = projectPageUrl;
			}
		});
	}, 1000);
});

/*miniSlider.destroy();
sliderMouseTracker.destroy();
slideshow.destroy();*/
