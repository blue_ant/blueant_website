let officeCoords = [55.8120755, 37.6258949];
let gmap = new GMaps({
	div: "#map",
	lat: officeCoords[0],
	lng: officeCoords[1],
	disableDefaultUI: true,
	zoomControl: true,
	zoom: 16,
	styles: [
		{
			featureType: "all",
			elementType: "labels.text.fill",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "40"
				}
			]
		},
		{
			featureType: "all",
			elementType: "labels.text.stroke",
			stylers: [
				{
					visibility: "off"
				},
				{
					lightness: "-73"
				}
			]
		},
		{
			featureType: "all",
			elementType: "labels.icon",
			stylers: [
				{
					visibility: "off"
				}
			]
		},
		{
			featureType: "administrative",
			elementType: "geometry.fill",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-35"
				}
			]
		},
		{
			featureType: "administrative",
			elementType: "geometry.stroke",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-20"
				},
				{
					weight: 1.2
				}
			]
		},
		{
			featureType: "landscape",
			elementType: "geometry",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "0"
				}
			]
		},
		{
			featureType: "poi",
			elementType: "geometry",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-5"
				}
			]
		},
		{
			featureType: "road",
			elementType: "geometry.stroke",
			stylers: [
				{
					visibility: "off"
				},
				{
					lightness: "80"
				}
			]
		},
		{
			featureType: "road.highway",
			elementType: "geometry.fill",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-30"
				}
			]
		},
		{
			featureType: "road.highway.controlled_access",
			elementType: "geometry.stroke",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-40"
				}
			]
		},
		{
			featureType: "road.arterial",
			elementType: "geometry",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-25"
				}
			]
		},
		{
			featureType: "road.local",
			elementType: "geometry",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-20"
				}
			]
		},
		{
			featureType: "transit",
			elementType: "geometry",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "-20"
				}
			]
		},
		{
			featureType: "water",
			elementType: "geometry",
			stylers: [
				{
					color: "#0071e0"
				},
				{
					lightness: "20"
				}
			]
		},
		{
			featureType: "landscape.man_made",
			elementType: "geometry.stroke",
			stylers: [
				{
					color: "#ffffff"
				},
				{
					lightness: "0"
				}
			]
		}
	]
});

gmap.addMarker({
	lat: officeCoords[0],
	lng: officeCoords[1],
	title: "Мы здесь!",
	infoWindow: {
		content: `<h5 style='font-size:120%;font-weight: 500;margin-bottom:10px;'>ООО &laquoСиний Муравей&raquo</h5>
	<div style="font-size:110%;"><p><strong>Адрес:</strong> ул.Звездный б-р ст.19 к.1</p></div>
	`
	},
	icon: {
		url: "/img/map_mark.png",
		scaledSize: new google.maps.Size(42, 42),
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(24, 19)
	}
});

new InteractiveForm(document.querySelector(".Form-contactUs"));