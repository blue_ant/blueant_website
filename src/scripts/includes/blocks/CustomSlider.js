class CustomSlider {
	constructor(el, opts, sliderMouseTracker) {
		this.opts = {
			slideChangeDuration: 1.5,
			w: 1920,
			h: 977,
			shockwaveWidth: 990.0
		};

		$.extend(this.opts, opts);

		if (sliderMouseTracker instanceof CursorSmoothTracker) {
			this.sliderMouseTracker = sliderMouseTracker;
		} else {
			console.error(
				"CustomSlider class constructor requires <CursorSmoothTracker> instance as the sliderMouseTracker parameter!"
			);
			return false;
		}

		this.activeIndex = 0;

		let $el = $(el);

		this.$headerTitle = $el.find(".CustomSlider_title");
		this.$slideCounter = $el.find(".CustomSlider_progressNum");

		this.renderer = new PIXI.autoDetectRenderer(this.opts.w, this.opts.h /*, { transparent: true }*/);
		this.renderer.view.className += "CustomSlider_bgRenderer";

		this.baseStage = new PIXI.display.Stage();
		this.baseStage.group.enableSort = true;

		{
			let axisShift = Math.ceil((this.opts.shockwaveWidth / 100) * 66.6);
			this.shockWaveFilter = new PIXI.filters.ShockwaveFilter(
				[this.opts.w + axisShift, this.opts.h + axisShift],
				{
					amplitude: 50.0,
					// brightness: 5,
					wavelength: this.opts.shockwaveWidth * 2,
					speed: Math.ceil(Math.hypot(this.opts.w, this.opts.h) + this.opts.shockwaveWidth * 2)
				},
				1
			);
		}

		this.baseStage.filters = [this.shockWaveFilter];
		this.$canvasWrap = $el.find(".CustomSlider_canvasWrap");
		this.$canvasWrap.append(this.renderer.view);

		this.progressBar = $el.find(".CustomSlider_progressBar").get(0);
		this.progressTimeline = new TimelineMax({
			paused: false
			// repeat: -1
		});

		this.progressTimeline
			.to(this.progressBar, 10, {
				delay: this.opts.slideChangeDuration,
				scaleY: 100,
				ease: Linear.easeNone
			})
			.set(this.progressBar, {
				scaleY: 0
			})
			.call(() => {
				if (this.opts.onProgressBarEnd) {
					this.opts.onProgressBarEnd();
				} else {
					this.slideNext();
				}
			})
			.repeat();
		this.$el = $el;

		this._initEtalonsilverSlide();
		this._initKotelnikiSlide();
		this._initSerebricaSlide();
		this._paused = false;

		this.animationLoopIDs = {};

		let _animate = () => {
			// console.log("base stage render loop at: " + Date.now());
			if (!this._paused) {
				this.renderer.render(this.baseStage);
			}
			this.animationLoopIDs.baseStageRender = requestAnimationFrame(_animate);
		};

		_animate();
	}

	play() {
		if (!this._paused) return false;
		this.$el.removeClass("CustomSlider-disabled");
		this._paused = false;
		this.progressTimeline.resume();
	}

	pause() {
		if (this._paused) return false;
		this.$el.addClass("CustomSlider-disabled");
		this._paused = true;
		this.progressTimeline.pause();
	}

	destroy() {
		this.progressTimeline.kill();
		for (let key in this.animationLoopIDs) {
			cancelAnimationFrame(this.animationLoopIDs[key]);
		}

		this.progressTimeline = null;
		this.baseStage.destroy();

		let slider = this.$el.get(0);
		let shade = slider.querySelector(".CustomSlider_shade");
		let linksBlock = slider.querySelectorAll(".CustomSlider_slideInfo")[this.activeIndex];
		let popupLinks = linksBlock.querySelectorAll(".CustomSlider_infoItem");
		let inner = slider.querySelector(".CustomSlider_inner");
		let readMoreBtn = slider.querySelector(".CustomSlider_circBtn-n1");

		let tl = new TimelineMax();
		tl.set([inner, readMoreBtn], {
			pointerEvents: "none"
		})
			.set(linksBlock, {
				autoAlpha: 1
			})
			.addLabel("animeStart")

			.to(
				shade,
				0.5,
				{
					autoAlpha: 1
				},
				"animeStart"
			)
			.to(
				this.$el.find(".CustomSlider_progress"),
				0.6,
				{
					x: -this.$el.find(".CustomSlider_headerBlock").offset().left - 50,
					autoAlpha: 0.3,
					delay: 0.5,
					ease: Back.easeIn.config(1)
				},
				"animeStart"
			)
			.staggerFromTo(
				popupLinks,
				0.15 * popupLinks.length,
				{
					autoAlpha: 0,
					y: "64px"
				},
				{
					autoAlpha: 1,
					y: "0px",
					delay: 0.1,
					ease: Back.easeOut.config(2.5)
				},
				0.1,
				"animeStart"
			);
	}

	_initEtalonsilverSlide() {
		let stage = new PIXI.display.Layer();
		stage.zIndex = 3;

		let fake3dLayer,
			fakeDepthFilter,
			fake3dSprite,
			frontSprite,
			fake3dLayer2,
			fakeDepthFilter2,
			fake3dSprite2,
			frontSprite2,
			cloudsLayer;

		let pixiLoader = new PIXI.loaders.Loader();

		let animate = () => {
			if (this.activeIndex === 0) {
				let mouseShiftX = this.sliderMouseTracker.easedPos.x;
				let mouseShiftY = this.sliderMouseTracker.easedPos.y;

				// START: sides animation
				fakeDepthFilter2.scale.x = -mouseShiftX / 100;
				fakeDepthFilter2.scale.y = -mouseShiftY / 80;
				frontSprite2.addChild(fake3dSprite2);
				// End: sides animation

				// START: front animation
				fakeDepthFilter.scale.x = -mouseShiftX / 30;
				fakeDepthFilter.scale.y = -mouseShiftY / 25;
				frontSprite.addChild(fake3dSprite);
				// End: front animation

				cloudsLayer.position.x = -mouseShiftX / 40;
				cloudsLayer.position.y = -mouseShiftY / 40;

				frontSprite.position.x = mouseShiftX / 100;
				frontSprite.position.y = mouseShiftY / 100;

				frontSprite2.position.x = mouseShiftX / 190;
				frontSprite2.position.y = mouseShiftY / 190;
			}

			this.animationLoopIDs.etalonsilver = requestAnimationFrame(animate);
		};

		let setScene = () => {
			// START: sky bg
			let frontSprite3 = new PIXI.Sprite(pixiLoader.resources.skyPic.texture);
			stage.addChild(frontSprite3);
			// END: sky bg

			// START: side houses
			frontSprite2 = new PIXI.Sprite(pixiLoader.resources.sidesPic.texture);
			fake3dLayer2 = frontSprite2;
			fake3dLayer2.addChild(frontSprite2);
			fake3dSprite2 = new PIXI.Sprite(pixiLoader.resources.sidesDepthMapPic.texture);
			fake3dSprite2.renderable = false;
			fakeDepthFilter2 = new PIXI.filters.DisplacementFilter(fake3dSprite2, 0);
			frontSprite2.filters = [fakeDepthFilter2];
			stage.addChild(fake3dLayer2);
			// END: side houses

			// START: front house
			frontSprite = new PIXI.Sprite(pixiLoader.resources.fgPic.texture);
			fake3dLayer = frontSprite;
			fake3dLayer.addChild(frontSprite);
			fake3dSprite = new PIXI.Sprite(pixiLoader.resources.frontDepthMapPic.texture);
			fake3dSprite.renderable = false;
			fakeDepthFilter = new PIXI.filters.DisplacementFilter(fake3dSprite, 0);
			frontSprite.filters = [fakeDepthFilter];
			stage.addChild(fake3dLayer);
			// END: front house

			// START: clouds overlay
			let frontSprite4 = new PIXI.Sprite(pixiLoader.resources.cloudsPic.texture);
			cloudsLayer = frontSprite4;

			stage.addChild(cloudsLayer);
			// END: clouds overlay

			stage.headerTitle = "Серебрянный Фонтан";
			this.baseStage.addChild(stage);

			animate();
		};

		pixiLoader.add("fgPic", "/img/slides/slide_1/front.png");
		pixiLoader.add("skyPic", "/img/slides/slide_1/sky.jpg");
		pixiLoader.add("sidesPic", "/img/slides/slide_1/side.png");
		pixiLoader.add("frontDepthMapPic", "/img/slides/slide_1/front_map.jpg");
		pixiLoader.add("sidesDepthMapPic", "/img/slides/slide_1/side_map.jpg");
		pixiLoader.add("cloudsPic", "/img/slides/slide_1/clouds.png");
		pixiLoader.once("complete", setScene);
		pixiLoader.load();
	}

	_initKotelnikiSlide() {
		let count = 0;
		let stage = new PIXI.display.Layer();

		let water, displacementSprite, displacementFilter;
		let pixiLoader = new PIXI.loaders.Loader();

		let animate = () => {
			if (this.activeIndex === 1) {
				displacementSprite.x = count * 10;
				displacementSprite.y = count * 10;

				count += 0.05;

				water.filters = [displacementFilter];

				let mouseShiftX = window.innerWidth / 2 - this.sliderMouseTracker.easedPos.x;
				let mouseShiftY = window.innerHeight / 2 - this.sliderMouseTracker.easedPos.y;
				stage.position.x = mouseShiftX / 150;
				stage.position.y = mouseShiftY / 135;
			}

			this.animationLoopIDs.kotelniki = requestAnimationFrame(animate);
		};

		let setScene = () => {
			water = new PIXI.Sprite(pixiLoader.resources.waterPic.texture);
			water.anchor.x = 0;

			displacementSprite = new PIXI.Sprite(pixiLoader.resources.waterMapPic.texture);
			displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;

			displacementFilter = new PIXI.filters.DisplacementFilter(displacementSprite);

			displacementSprite.scale.y = 0.6;
			displacementSprite.scale.x = 0.6;
			stage.addChild(displacementSprite);
			stage.addChild(water);

			let beachMaskSprite = new PIXI.Sprite(pixiLoader.resources.beachMaskPic.texture);
			let beachMask = beachMaskSprite;

			stage.addChild(beachMask);

			stage.headerTitle = "Новые Котельники";
			this.baseStage.addChild(stage);
			animate();
		};

		pixiLoader.add("waterPic", "/img/slides/slide_2/water.jpg");
		pixiLoader.add("waterMapPic", "/img/slides/slide_2/water_map.jpg");
		pixiLoader.add("beachMaskPic", "/img/slides/slide_2/beach.png");

		pixiLoader.once("complete", setScene);
		pixiLoader.load();
	}

	_initSerebricaSlide() {
		let stage = new PIXI.display.Layer();

		let dayPicLayer, fakeDepthFilter, displaceMapSprite, dayPicSprite, nightPicLayer, nightPicSprite;

		let pixiLoader = new PIXI.loaders.Loader();

		let animate = () => {
			if (this.activeIndex === 2) {
				let mouseShiftX = this.sliderMouseTracker.easedPos.x;
				let mouseShiftY = this.sliderMouseTracker.easedPos.y;

				// START: displacement map update
				fakeDepthFilter.scale.x = -mouseShiftX / 180;
				fakeDepthFilter.scale.y = -mouseShiftY / 155;
				dayPicSprite.addChild(displaceMapSprite);
				nightPicSprite.addChild(displaceMapSprite);
				// End: displacement map update

				dayPicSprite.position.x = nightPicSprite.position.x = mouseShiftX / 100;
				dayPicSprite.position.y = nightPicSprite.position.y = mouseShiftY / 100;

				let cursorHpercent = this.sliderMouseTracker.easedPos.absY / (window.innerHeight / 100);
				dayPicSprite.alpha = cursorHpercent / 100;
			}

			this.animationLoopIDs.serebrica = requestAnimationFrame(animate);
		};

		let setScene = () => {
			// START: night house
			nightPicSprite = new PIXI.Sprite(pixiLoader.resources.nightPic.texture);
			nightPicLayer = nightPicSprite;
			nightPicLayer.addChild(nightPicSprite);
			displaceMapSprite = new PIXI.Sprite(pixiLoader.resources.depthMapPic.texture);
			fakeDepthFilter = new PIXI.filters.DisplacementFilter(displaceMapSprite, 0);
			nightPicSprite.filters = [fakeDepthFilter];
			stage.addChild(nightPicLayer);
			// END: night house

			// START: day house
			dayPicSprite = new PIXI.Sprite(pixiLoader.resources.dayPic.texture);
			dayPicLayer = dayPicSprite;
			dayPicLayer.addChild(dayPicSprite);
			displaceMapSprite = new PIXI.Sprite(pixiLoader.resources.depthMapPic.texture);
			fakeDepthFilter = new PIXI.filters.DisplacementFilter(displaceMapSprite, 0);
			dayPicSprite.filters = [fakeDepthFilter];
			stage.addChild(dayPicLayer);
			// END: day house

			stage.headerTitle = "Серебрица";
			this.baseStage.addChild(stage);
			animate();
		};

		pixiLoader.add("nightPic", "/img/slides/slide_3/night.jpg");
		pixiLoader.add("dayPic", "/img/slides/slide_3/day.jpg");
		pixiLoader.add("depthMapPic", "/img/slides/slide_3/map.jpg");

		pixiLoader.once("complete", setScene);
		pixiLoader.load();
	}

	slideNext() {
		this.progressTimeline.restart();

		let slides = this.baseStage.children;

		let nextSlideIndex = this.activeIndex + 1;

		if (nextSlideIndex >= slides.length) {
			nextSlideIndex = 0;
		}

		let prevSlide = slides[this.activeIndex];
		let nextSlide = slides[nextSlideIndex];

		nextSlide.renderable = true;

		prevSlide.zIndex = 2;
		nextSlide.zIndex = 3;

		{
			let otherSlides = _.filter(slides, s => {
				let result = true;
				result = s === prevSlide || s === nextSlide;
				return !result;
			});

			otherSlides.forEach(s => {
				s.zIndex = 0;
			});

			otherSlides.forEach(sld => {
				sld.renderable = false;
			});
		}

		let animationProgress = {
			diagonal: 0
		};

		let holeMask = new PIXI.Graphics();

		let tl = new TimelineMax();
		this.baseStage.filters = [this.shockWaveFilter];

		this.$slideCounter.html(nextSlideIndex < 10 ? "0" + (nextSlideIndex + 1) : nextSlideIndex + 1);

		tl.addLabel("animeStart")
			.to(
				this.$canvasWrap,
				0.45,
				{
					scale: 1.08,
					ease: Power1.easeOut,
					onComplete: () => {
						TweenLite.to(this.$canvasWrap, 0.45, {
							ease: Power1.easeOut,
							scale: 1,
							delay: 0.1
						});
					}
				},
				"animeStart"
			)
			.to(
				animationProgress,
				this.opts.slideChangeDuration,
				{
					diagonal: Math.hypot(this.opts.w, this.opts.h),
					onUpdate: () => {
						holeMask.clear();
						holeMask.beginFill("#000000");
						holeMask.drawCircle(this.opts.w, this.opts.h, animationProgress.diagonal);
						nextSlide.mask = holeMask;
					},
					onComplete: () => {
						this.baseStage.filters = [];
						nextSlide.mask = null;
						holeMask.clear();
						prevSlide.renderable = false;
					}
				},
				"animeStart"
			)
			.fromTo(
				this.shockWaveFilter,
				this.opts.slideChangeDuration,
				{
					time: 0
				},
				{
					time: 1
				},
				"animeStart"
			)

			.to(
				this.$headerTitle,
				this.opts.slideChangeDuration / 4,
				{
					bottom: this.$headerTitle.height() / 2,
					opacity: 0,
					onComplete: () => {
						this.$headerTitle.html(nextSlide.headerTitle);

						TweenLite.fromTo(
							this.$headerTitle,
							this.opts.slideChangeDuration / 4,
							{
								bottom: -(this.$headerTitle.height() / 2),
								opacity: 0
							},
							{
								bottom: 0,
								opacity: 1
							}
						);
					}
				},
				"animeStart"
			);
		this.activeIndex = nextSlideIndex;
	}
}
