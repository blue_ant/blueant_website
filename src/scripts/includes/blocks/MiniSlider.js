class MiniSlider {
	constructor(el, opts = {}) {
		let $el = $(el);
		if ($el.lenght === 0) {
			console.error("Minislider class constructor can't find given target element");
			return;
		}

		let swiperOpts = {
			resistanceRatio: 0,
			allowTouchMove: false,
			loop: true,
			runCallbacksOnInit: false
		};

		let swiper = new Swiper(el, swiperOpts);

		if (!opts.onClick) {
			$el.on("click", event => {
				event.preventDefault();
				this.slideNext();
			});
		} else {
			$el.on("click", opts.onClick);
		}

		let nextSlideAnimeTimeline = new TimelineLite({
			paused: true,
			onComplete: () => {
				swiper.slideNext();
				TweenLite.set(this.$el, {
					clearProps: "cursor",
					delay: 0.85
				});
			}
		});

		nextSlideAnimeTimeline.to($el, 0.15, { scale: 0.88, cursor: "wait" }).to($el, 0.15, { scale: 1 });

		this.$el = $el;
		this.swiper = swiper;
		this.nextSlideAnimeTimeline = nextSlideAnimeTimeline;
	}

	slideNext() {
		this.nextSlideAnimeTimeline.restart();
	}

	destroy() {
		this.$el.off("click").css("pointer-events", "none");
		TweenLite.to(this.$el, 0.6, {
			delay: 0.45,
			y: "200px",
			ease: Back.easeIn.config(2.5),
			onComplete: () => {
				this.swiper.destroy(true, false);
			}
		});
	}
}
