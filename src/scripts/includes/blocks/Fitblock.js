let fitblocksCounter = 0;

class Fitblock {
	constructor(el) {
		this.$el = $(el);
		this.outer = this.$el.get(0);
		this.inner = this.outer.getElementsByClassName("Fitblock_inner")[0];
		this.pusher = this.inner.getElementsByClassName("Fitblock_pusher")[0];
		this.id = fitblocksCounter;

		if (this.pusher.complete || this.pusher.readyState === 4) {
			this.inner.style.visibility = "visible";
			this.refresh();
		} else {
			$(this.pusher).on("load", () => {
				this.inner.style.visibility = "visible";
				this.refresh();
			});
		}

		$(window).on(
			`resize.fitblock-${this.id}`,
			_.debounce(() => {
				this.refresh();
			}, 100)
		);

		fitblocksCounter++;
	}

	refresh() {
		let outer = this.outer;
		let inner = this.inner;
		let pusher = this.pusher;
		let wrapH = outer.offsetHeight;
		let wrapW = outer.offsetWidth;

		let pusherH = pusher.naturalHeight || pusher.width;
		let pusherW = pusher.naturalWidth || pusher.height;

		let rel = pusherW / pusherH;
		if (wrapW / pusherW > wrapH / pusherH) {
			pusher.style.width = `${wrapW}px`;
			pusher.style.height = "auto";
			inner.style.marginLeft = `-${wrapW / 2}px`;
			inner.style.marginTop = `-${wrapW / rel / 2}px`;
		} else {
			pusher.style.width = "auto";
			pusher.style.height = `${wrapH}px`;
			inner.style.marginLeft = `-${(wrapH * rel) / 2}px`;
			inner.style.marginTop = `-${wrapH / 2}px`;
		}
	}

	destroy() {
		$(window).off(`.fitblock-${this.id}`);
	}
}
