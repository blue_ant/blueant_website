let cursorSmoothTrackersCount = 1;
class CursorSmoothTracker {
	constructor(trackEl) {
		let $trackEl = $(trackEl);
		let mousePos = {
			x: window.innerWidth / 2,
			y: window.innerHeight / 2
		};

		this.id = cursorSmoothTrackersCount;
		cursorSmoothTrackersCount++;

		$trackEl.on(`mousemove.smoothtracker${this.id}`, event => {
			mousePos.x = event.pageX;
			mousePos.y = event.pageY;
		});

		let easedPos = {
			x: window.innerWidth / 2,
			y: window.innerHeight / 2,
			absX: window.innerWidth / 2,
			absY: window.innerHeight / 2
		};

		this.calculateFunc = function() {
			TweenLite.to(easedPos, 1, {
				x: window.innerWidth / 2 - mousePos.x,
				y: window.innerHeight / 2 - mousePos.y,
				absX: mousePos.x,
				absY: mousePos.y,
				ease: Power2.easeOut
			});
		};

		TweenLite.ticker.addEventListener("tick", this.calculateFunc);

		this.easedPos = easedPos;
		this.$trackEl = $trackEl;
	}

	destroy() {
		TweenLite.ticker.removeEventListener("tick", this.calculateFunc);
		this.$trackEl.off(`mousemove.smoothtracker${this.id}`);
	}
}
