class Menu {
	constructor(el, opts = {}) {
		let $el = $(el);
		if ($el.lenght === 0) {
			console.error("Menu class constructor can't find given target element");
			return false;
		}
		let $grid = $el.find(".Menu_grid");
		let $groups = $grid.find(".Menu_group");
		let $groupContents = $groups.find(".Menu_groupContent");
		let $sidebar = $(".Sidebar");
		let $sidebarBurger = $sidebar.find(".Sidebar_burgerButton");
		let $topbar = $(".Topbar");
		let $topbarBurger = $topbar.find(".Topbar_burger");
		let $topbarBurgerIco = $topbarBurger.find(".SvgIco-burgerMenu");

		$el.find(".Menu_inner").on("scroll", function(event) {
			event.preventDefault();
			event.stopPropagation();
			return false;
		});

		document.body.addEventListener("keydown", event => {
			if (event.keyCode === 27 && this.isOpen) {
				this.hide();
			}
		});

		$groups
			.hover(
				// for desktops
				event => {
					if (window.matchMedia("(min-width: 1365px)").matches) {
						this.highlightGroup($(event.currentTarget).find(".Menu_groupContent"));

						const item = $(event.currentTarget).find(".Menu_item");

						TweenMax.set(item, {
							y: 20,
							opacity: 0
						});

						TweenMax.staggerTo(
							item,
							0.4,
							{
								opacity: 1,
								y: 0
							},
							0.1
						);

						TweenMax.to(
							$groupContents
								.not($(event.currentTarget).find(".Menu_groupContent"))
								.find(".Menu_groupHead"),
							0.3,
							{
								opacity: 0.5,
								scale: 0.9
							}
						);
					}
				},
				event => {
					if (window.matchMedia("(min-width: 1365px)").matches) {
						this.resetGroups();

						const item = $(event.currentTarget).find(".Menu_item");

						TweenMax.staggerTo(
							item,
							0.2,
							{
								opacity: 0,
								y: 20
							},
							0.1
						);

						TweenMax.to($groupContents.not(event.currentTarget).find(".Menu_groupHead"), 0.3, {
							opacity: 1,
							scale: 1
						});
					}
				}
			)
			.on("click", event => {
				// for mobiles

				if (window.matchMedia("(max-width: 1365px)").matches && !event.target.closest(".Menu_linksList")) {
					let $target = $(event.currentTarget);
					let $groupName = $target.find(".Menu_groupName");
					if (!$groupName.is("a")) {
						event.preventDefault();
						this.toggleGroup($target.find(".Menu_groupContent"));
					}
				}
			});

		this.isOpen = false;
		this.$el = $el;
		this.$groups = $groups;
		this.$groupContents = $groupContents;
		this.$sidebar = $sidebar;
		this.$sidebarBurger = $sidebarBurger;
		this.$topbar = $topbar;
		this.$topbarBurger = $topbarBurger;
		this.$topbarBurgerIco = $topbarBurgerIco;
		this.opts = opts;
	}

	resetGroups(groupCont) {
		requestAnimationFrame(() => {
			this.$groupContents.removeClass("Menu_groupContent-faded Menu_groupContent-active");
		});

		if (!window.matchMedia("(min-width: 1365px)").matches) {
			groupCont.find(".Menu_linksList").slideUp(300);

			TweenMax.staggerTo(
				groupCont.find(".Menu_item"),
				0.5,
				{
					delay: 0.3,
					opacity: 0,
					y: -20
				},
				0.2
			);
		}
	}

	highlightGroup(groupCont) {
		requestAnimationFrame(() => {
			this.$groupContents
				.removeClass("Menu_groupContent-active")
				.not(groupCont)
				.addClass("Menu_groupContent-faded");

			groupCont.removeClass("Menu_groupContent-faded");
			groupCont.addClass("Menu_groupContent-active");
		});
		if (!window.matchMedia("(min-width: 1365px)").matches) {
			groupCont.find(".Menu_linksList").slideDown(300);
			this.$groupContents
				.not(groupCont)
				.find(".Menu_linksList")
				.slideUp(300);

			TweenMax.set($(groupCont).find(".Menu_item"), {
				y: -20,
				opacity: 0
			});

			TweenMax.staggerTo(
				$(groupCont).find(".Menu_item"),
				0.5,
				{
					delay: 0.3,
					opacity: 1,
					y: 0
				},
				0.2
			);
		}
	}

	toggleGroup(groupCont) {
		if (groupCont.hasClass("Menu_groupContent-active")) {
			this.resetGroups(groupCont);
		} else {
			this.highlightGroup(groupCont);
		}
	}

	hide() {
		if (!this.isOpen) return false;
		this.$el.trigger("mainmenuhide");
		const $num = this.$groupContents.find(".Menu_groupNum");
		const closeTimeline = new TimelineMax();

		if (!window.matchMedia("(max-width: 767px)").matches) {
			closeTimeline
				.staggerTo(
					this.$groupContents,
					0.6,
					{
						x: 0,
						y: 0,
						opacity: 0,
						scale: 1,
						ease: Back.easeIn,
						onComplete: () => this.$sidebar.removeClass("Sidebar-inverted")
					},
					0.1,
					"-=0.3"
				)

				.to(
					this.$groups,
					0.3,
					{
						borderRightColor: "#0071e0"
					},
					"-=0.35"
				)

				.staggerTo(
					$num,
					0.4,
					{
						scale: 1.5
					},
					0.1,
					"-=0.5"
				)

				.to(
					this.$el,
					0.5,
					{
						x: -parseInt(this.$el.outerWidth(true))
					},
					"-=0.4"
				)

				.to(
					this.$sidebarBurger.find(".Sidebar_burgerIcoRotate"),
					0.4,
					{
						rotation: 0,
						onStart: () =>
							this.$sidebarBurger
								.find(".Sidebar_burgerIcoStatic")
								.removeClass("Sidebar_burgerIcoStatic-active")
					},
					"-=0.4"
				)

				.to($("body"), 0.1, {
					onComplete: () => {
						this.$sidebarBurger.removeClass("clickPrevent");
						this.$topbarBurger.removeClass("clickPrevent");
						this.isOpen = false;
						$.fancybox.close();
					}
				});
		} else {
			closeTimeline.set(this.$groupContents, {
				y: -100,
				opacity: 0,
				scale: 1
			});

			closeTimeline
				.to(this.$topbarBurgerIco, 0.5, {
					rotation: 0
				})

				.to(
					this.$groupContents,
					0,
					{
						y: -100,
						opacity: 0
					},
					0.2,
					"-=0.3"
				)

				.to(
					this.$el,
					0.5,
					{
						y: -parseInt(this.$el.outerHeight(true))
					},
					"-=0.75"
				)

				.to($("body"), 0.1, {
					onComplete: () => {
						this.$sidebarBurger.removeClass("clickPrevent");
						this.$topbarBurger.removeClass("clickPrevent");
						this.isOpen = false;
						$.fancybox.close();
					}
				});
		}
	}

	show() {
		if (this.isOpen) return false;
		$.fancybox.open(this.$el, {
			arrows: false,
			infobar: false,
			touch: false,
			buttons: [],
			smallBtn: false,
			animationDuration: 280,
			modal: true,
			baseTpl:
				'<div class="fancybox-container fancybox-container--underTopbar" role="dialog" tabindex="-1">' +
				'<div class="fancybox-stage"></div>' +
				"</div>"
		});
		this.isOpen = true;

		this.$el.trigger("mainmenushow");

		const $num = this.$groupContents.find(".Menu_groupNum");

		const showTimeline = new TimelineMax();

		if (!window.matchMedia("(max-width: 767px)").matches) {
			showTimeline.set(this.$sidebarBurger.find(".Sidebar_burgerIcoRotate"), {
				rotation: 0
			});

			showTimeline.set(this.$groups, {
				borderRightColor: "#0071e0"
			});

			showTimeline.set(this.$el, {
				x: -parseInt(this.$el.outerWidth(true)),
				y: 0
			});

			showTimeline.set(this.$groupContents, {
				x: 0,
				y: 0,
				opacity: 0
			});

			showTimeline.set($num, {
				scale: 1.5
			});

			showTimeline

				.to(this.$sidebarBurger.find(".Sidebar_burgerIcoRotate"), 1, {
					rotation: 270,
					onStart: () =>
						this.$sidebarBurger.find(".Sidebar_burgerIcoStatic").addClass("Sidebar_burgerIcoStatic-active")
				})

				.to(
					this.$el,
					1.2,
					{
						x: 0
					},
					"-=0.75"
				)

				.to(
					this.$groups,
					0.3,
					{
						borderRightColor: "#0061c1"
					},
					"-=0.4"
				)

				.staggerTo(
					this.$groupContents,
					0.7,
					{
						x: 0,
						y: 0,
						opacity: 1,
						scale: 1,
						ease: Back.easeIn
					},
					0.1,
					"-=0.9"
				)

				.to(
					this.$sidebar,
					0.25,
					{
						onComplete: () => this.$sidebar.addClass("Sidebar-inverted")
					},
					"-=0.9"
				)

				.staggerTo(
					$num,
					0.4,
					{
						scale: 1
					},
					0.1,
					"-=0.3"
				)

				.to($("body"), 0.1, {
					onComplete: () => {
						this.$sidebarBurger.removeClass("clickPrevent");
						this.$topbarBurger.removeClass("clickPrevent");
					}
				});
		} else {
			showTimeline.set(this.$topbarBurgerIco, {
				rotation: 0
			});

			showTimeline.set(this.$el, {
				y: -400,
				x: 0
			});

			showTimeline.set($num, {
				clearProps: "all"
			});

			showTimeline.set(this.$groupContents, {
				y: -100,
				x: 0,
				opacity: 0,
				scale: 1.1
			});

			showTimeline
				.to(this.$topbarBurgerIco, 0.8, {
					rotation: 90
				})

				.to(
					this.$el,
					0.5,
					{
						y: 0
					},
					"-=0.75"
				)

				.staggerTo(
					this.$groupContents,
					0.3,
					{
						y: 0,
						opacity: 1,
						scale: 1
					},
					0.2,
					"-=0.3"
				)

				.to($("body"), 0.1, {
					onComplete: () => {
						this.$sidebarBurger.removeClass("clickPrevent");
						this.$topbarBurger.removeClass("clickPrevent");
					}
				});
		}
	}

	toggle() {
		this.$sidebarBurger.addClass("clickPrevent");
		this.$topbarBurger.addClass("clickPrevent");

		if (this.isOpen) {
			this.hide();
		} else {
			this.show();
		}
	}
}
