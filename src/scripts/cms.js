const ps = new PerfectScrollbar(".FeaturePageLayout_secondScroller", {
	wheelSpeed: 2,
	wheelPropagation: true,
	minScrollbarLength: 20,
	suppressScrollX: true
});

$(window).on(
	"resize",
	_.debounce(event => {
		event.preventDefault();
		ps.update();
	}, 100)
);
