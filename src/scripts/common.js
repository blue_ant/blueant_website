/*=require ./includes/blocks/*.js */
/*=require ./includes/chunks/fancybox_defaults.js*/

// START: Main menu initialization
{
	let $sidebar = $(".Sidebar");
	let $sidebarBurger = $sidebar.find(".Sidebar_burgerButton");
	let $topbar = $(".Topbar");
	let $topbarBurger = $topbar.find(".Topbar_burger");

	var mainMenu = new Menu(".Menu");

	$sidebarBurger.add($topbarBurger).on("click", event => {
		event.preventDefault();
		mainMenu.toggle();
	});
}

$(window).on("load", () => {
	window.pagePreloader = new Preloader("#pagePreloader");
	window.pagePreloader.hide();
});

// END: Main menu initialization
